package main

import (
	server "RPSPL/internal"
	"os"
)

func main() {
	port, exists := os.LookupEnv("PORT")
	if !exists {
		port = ":8080"
	} else {
		port = ":" + port
	}

	server.HandleRequests(port)
}
