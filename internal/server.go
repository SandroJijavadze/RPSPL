package server

// We will implement server endpoints here.
import (
	"RPSPL/internal/game"
	"encoding/json"
	"log"
	"net/http"
	"os"
)

func logRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

// HandleRequests sets up all the handlers for required endpoints
func HandleRequests(port string) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// I know this is not a best practice, just want to wrap up cause I don't think I'll have time in coming days.
		dir, _ := os.Getwd()
		http.ServeFile(w, r, dir+"/internal/static/index.html")
	})
	http.HandleFunc("/choices", choices)
	http.HandleFunc("/choice", choice)
	http.HandleFunc("/play", play)

	log.Fatal(http.ListenAndServe(port, logRequest(http.DefaultServeMux)))
}

func choices(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(game.AvailableChoices())
	if err != nil {
		log.Println(err)
		w.WriteHeader(422)
		_, err = w.Write([]byte(`"status": "Unprocessable Entity"`))
		log.Println("could not write response body for status 422 error " + err.Error())
		return
	}
}

func choice(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	id, choice := game.RandomChoice()
	res := game.ChoiceResult{ID: id, Name: choice}

	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		log.Println(err)
		w.WriteHeader(422)
		_, err = w.Write([]byte(`"status": "error"`))
		log.Println("could not write response body for status error " + err.Error())
		return
	}
}

func play(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method != http.MethodPost {
		w.WriteHeader(405)
		_, err := w.Write([]byte(`"status": "method not allowed"`))
		log.Println("could not write response body for status 405 error " + err.Error())
		return
	}

	type req struct {
		Player int `json:"player"`
	}

	decoder := json.NewDecoder(r.Body)
	var reqBody req

	err := decoder.Decode(&reqBody)
	if err != nil {
		log.Println(err)
		w.WriteHeader(400)
		_, err := w.Write([]byte(`"status": "bad request"`))
		log.Println("could not write response body for status 400 error " + err.Error())
		return
	}

	gameEng := game.NewGame()

	serverChoice, gameResult := gameEng.Play(game.IntToChoice(reqBody.Player))

	type Response struct {
		Results  string `json:"results"`
		Player   int    `json:"player"`
		Computer int    `json:"computer"`
	}
	resp := Response{gameResult, reqBody.Player, serverChoice}

	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		log.Println(err)
		w.WriteHeader(422)
		_, err := w.Write([]byte(`"status": "Unprocessable Entity"`))
		log.Println("could not write response body for status 422 error " + err.Error())
		return
	}
}
