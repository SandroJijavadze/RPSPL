package server

import (
	"RPSPL/internal/game"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

func TestChoices(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/choices", nil)
	w := httptest.NewRecorder()
	choices(w, req)
	res := w.Result()
	defer func() {
		err := res.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	expected := game.AvailableChoices()
	result := []game.ChoiceResult{}

	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		log.Fatalln(err)
	}

	if !reflect.DeepEqual(result, expected) {
		t.Errorf("expected %q got %v", expected, result)
	}
}

func TestChoice(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/choice", nil)
	w := httptest.NewRecorder()
	choice(w, req)
	res := w.Result()
	defer func() {
		err := res.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	expected := game.AvailableChoices()
	var result game.ChoiceResult

	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		log.Fatalln(err)
	}

	foundMatch := false

	for _, v := range expected {
		if reflect.DeepEqual(result, v) {
			foundMatch = true
			break
		}
	}

	if !foundMatch {
		t.Errorf("could not find matching random choice for result %v", result)
	}
}

func TestPlay(t *testing.T) {
	req := httptest.NewRequest(http.MethodPost, "/play", strings.NewReader("{'player':1}"))
	w := httptest.NewRecorder()
	choice(w, req)
	res := w.Result()
	defer func() {
		err := res.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	type Result struct {
		Results  string `json:"results"`
		Player   int    `json:"player"`
		Computer int    `json:"computer"`
	}
	var result Result
	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		log.Fatalln(err)
	}

	gameEng := game.NewGameWithChoice(game.IntToChoice(1))

	serverChoice, gameRes := gameEng.Play(game.IntToChoice(1))
	if serverChoice != 1 || gameRes != "tie" {
		t.Errorf("test input with computer choice 1 and player choice 1, expected computerchoice 1 and gameres tie, got  computerchoice %d and gameres %s", serverChoice, gameRes)
	}
}
