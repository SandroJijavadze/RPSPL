package game

import (
	"math"
	"math/rand"
)

type choice int

// choices enum
const (
	Rock choice = iota
	Paper
	Scissors
	Spock
	Lizard
)
const numChoices = 5

// ChoiceMaker represents the ability to generate one of the available game choices
type ChoiceMaker interface {
	makeChoice() choice
}

// Game represents the RPSPL engine, it has ChoiceMaker and ability to test arbitrary inputs with regard to the ChoiceMaker.
type Game struct {
	ChoiceMaker
}

type randomChoiceMaker struct{}

// makeChoice generates random choice
func (cm randomChoiceMaker) makeChoice() choice {
	randChoice := choice(rand.Intn(numChoices))
	return randChoice
}

// ChoiceMakerMock is a mock for ChoiceMaker computer decision will always match desired choice
type ChoiceMakerMock struct {
	DesiredChoice string
}

func (c ChoiceMakerMock) makeChoice() choice {
	return getChoice(c.DesiredChoice)
}

// NewGame returns new Game with randomChoiceMaker
func NewGame() Game {
	return Game{randomChoiceMaker{}}
}

// NewGameWithChoice returns new Game with mocked choiceMaker
func NewGameWithChoice(choice string) Game {
	return Game{ChoiceMakerMock{choice}}
}

// String is used to satisfy Stringer interface.
func (c choice) String() string {
	switch c {
	case Rock:
		return "rock"
	case Paper:
		return "paper"
	case Scissors:
		return "scissor"
	case Spock:
		return "spock"
	case Lizard:
		return "lizard"
	}
	return "unknown"
}

// getChoice returns choice represented by string.
func getChoice(c string) choice {
	switch c {
	case "rock":
		return Rock
	case "paper":
		return Paper
	case "scissor":
		return Scissors
	case "spock":
		return Spock
	case "lizard":
		return Lizard
	}
	return -1
}

// winsAgainst tests ability of one choice to win against another
func (c choice) winsAgainst(otherChoice choice) bool {
	switch c {
	case Rock:
		return otherChoice == Scissors || otherChoice == Lizard
	case Paper:
		return otherChoice == Rock || otherChoice == Spock
	case Scissors:
		return otherChoice == Paper || otherChoice == Lizard
	case Spock:
		return otherChoice == Scissors || otherChoice == Rock
	case Lizard:
		return otherChoice == Paper || otherChoice == Spock
	default:
		return false
	}
}

// Play is returns the choice of Game and result with respect to input
func (game Game) Play(playerChoice string) (serverChoice int, result string) {

	userChoice := getChoice(playerChoice)
	gameChoice := game.makeChoice()

	if userChoice.winsAgainst(gameChoice) {
		return int(gameChoice), "win"
	}

	if gameChoice.winsAgainst(userChoice) {
		return int(gameChoice), "lose"
	}

	return int(gameChoice), "tie"
}

// RandomChoice uses
func RandomChoice() (id int, c string) {
	randChoice := randomChoiceMaker{}.makeChoice()
	return int(randChoice), randChoice.String()
}

// IntToChoice returns choice based on integer
func IntToChoice(i int) (c string) {
	randChoice := int(math.Abs(float64(i)))
	var resultChoice = choice(randChoice % numChoices)
	return resultChoice.String()
}

// ChoiceResult is the resulting struct for any kind of choice queries (random or other)
type ChoiceResult struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// AvailableChoices returns a list of available choices
func AvailableChoices() []ChoiceResult {
	return []ChoiceResult{
		{int(Rock), Rock.String()},
		{int(Paper), Paper.String()},
		{int(Scissors), Scissors.String()},
		{int(Spock), Spock.String()},
		{int(Lizard), Lizard.String()},
	}
}
