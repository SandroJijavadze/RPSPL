package game

import "testing"

type testCustomInput struct {
	userChoice     string
	serverChoice   int
	expectedResult string
}

func TestGame(t *testing.T) {
	tests := []testCustomInput{
		{"paper", int(getChoice("rock")), "win"},
		{"paper", int(getChoice("paper")), "tie"},
		{"rock", int(getChoice("scissor")), "win"},
		{"paper", int(getChoice("scissor")), "lose"},
		{"scissor", int(getChoice("lizard")), "win"},
		{"lizard", int(getChoice("scissor")), "lose"},
		{"spock", int(getChoice("spock")), "tie"},
		{"spock", int(getChoice("paper")), "lose"},
		//and the rest of cases
	}

	for _, v := range tests {
		game := NewGame()
		game.ChoiceMaker = ChoiceMakerMock{IntToChoice(v.serverChoice)}

		serverChoice, result := game.Play(v.userChoice)

		if result != v.expectedResult || serverChoice != v.serverChoice {
			t.Errorf("we have %q against %q, want %q, got %q", v.userChoice, v.serverChoice, v.expectedResult, result)
		}
	}

}

func TestRandomChoice(t *testing.T) {
	choiceID, randChoice := RandomChoice()
	if choiceID > numChoices || choiceID < 0 {
		t.Errorf("expected choiceID between 0 and numChoices, got %q", choiceID)
	}

	realChoice := getChoice(randChoice)
	if realChoice == -1 { // Not one of the enummed values
		t.Errorf("%q is not a valid random choice", randChoice)
	}

}

func TestIntToChoice(t *testing.T) {
	testInputs := []int{0, 1, 11, 111, -1, -15, -111}

	for _, inp := range testInputs {
		realChoice := IntToChoice(inp)
		strChoice := getChoice(realChoice)
		if strChoice == -1 { // Not one of the enummed values
			t.Errorf("%q is not a valid choice for input %d", realChoice, inp)
		}
	}

}
