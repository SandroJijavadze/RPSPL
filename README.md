#  Rock-paper-scissors-lizard-Spock
Rock-paper-scissors-lizard-Spock as defined on http://www.samkass.com/theories/RPSSL.html


### Building
`docker build -t rpspl .`
### Running
` docker run -p 8080:8080 rpspl `

Alternatively you can use image straight from gitlab registry after logging into gitlab registry (`docker login registry.gitlab.com`)

`docker run -p 8080:8080 registry.gitlab.com/sandrojijavadze/rpspl:latest`

Also, I just decided to copy static index file to not mess with CORS. 

## References
This repository uses original static index page from https://codechallenge.boohma.com 

